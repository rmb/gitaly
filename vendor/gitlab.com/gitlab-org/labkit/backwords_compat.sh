#!/usr/bin/env sh

set -eux

repo=$1
cloneDir=$(mktemp -d)
CI_PROJECT_DIR=${CI_PROJECT_DIR:-$(pwd)}

git clone --depth 1 "$repo" "$cloneDir"
cd "$cloneDir"

go mod edit -replace=gitlab.com/gitlab-org/labkit="$CI_PROJECT_DIR"

# Ensure go.mod and go.sum are up to date in the cloned repo, otherwise build may fail.
go mod tidy

make -j "$(nproc)"

rm -rf "$cloneDir"
