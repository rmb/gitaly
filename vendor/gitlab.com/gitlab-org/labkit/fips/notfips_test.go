//go:build !fips
// +build !fips

package fips

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCheck(t *testing.T) {
	// Testing log output is racy with logrus, but calling this is
	// useful to ensure FIPS and non-FIPS systems have this implemented.
	require.False(t, Check())
}

func TestEnabledk(t *testing.T) {
	require.False(t, Enabled())
}
